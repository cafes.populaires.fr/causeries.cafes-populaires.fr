// Récupère tous les éléments de tabcontent et les cache
let tabcontent = document.getElementsByClassName("tabcontent");
for (let i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
}

// Récupère tous les éléments tablinks et supprime la classe "active"
let tablinks = document.getElementsByClassName("tablinks");
for (let i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
}

// Affiche le premier onglet par défaut
document.getElementsByClassName('tabcontent')[0].style.display = "block";
document.getElementsByClassName('tablinks')[0].className += " active";

// Attache un gestionnaire d'événement click à chaque onglet
for (let i = 0; i < tablinks.length; i++) {
    tablinks[i].addEventListener('click', function(e) {
        // Empêche le comportement par défaut du clic (la navigation vers le lien)
        e.preventDefault();

        // Cache tous les contenus d'onglets
        for (let j = 0; j < tabcontent.length; j++) {
            tabcontent[j].style.display = "none";
        }

        // Supprime la classe "active" de tous les onglets
        for (let j = 0; j < tablinks.length; j++) {
            tablinks[j].className = tablinks[j].className.replace(" active", "");
        }

        // Affiche le contenu de l'onglet cliqué et ajoute la classe "active" à l'onglet cliqué
        document.getElementById(this.href.split('#')[1]).style.display = "block";
        this.className += " active";
    });
}
