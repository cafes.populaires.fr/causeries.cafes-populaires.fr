# Site pour permettre les débats de manières autonomes

Ce site a pour objet de permettre des débats de qualité avec des outils référencés.


Parmi les règles proposées il y en a au moins trois principales règles :
- Respecter ses interlocuteurs et interlocutrices.
- Attendre son tour de parole donné par l'animateur ou l'animatrice.
- Ne s'exprimer que pour soi, ni pourun parti ni pour une autre personne.
